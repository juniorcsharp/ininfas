# -*- coding: utf-8 -*-
__author__ = 'Junior Rodrigues'

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from yawdadmin import admin_site
from ninfas.models import *

admin.autodiscover()
admin_site._registry.update(admin.site._registry)
admin_site.register_top_menu_item('auth', icon_class="icon-th")
admin_site.register_top_menu_item('ninfas', icon_class="icon-th")
admin_site.register_top_menu_item('publicidades', icon_class="icon-th")
admin_site.register_top_menu_item('mensalidades', icon_class="icon-th")

import settings

urlpatterns = patterns('',
    url(r'^admin/', include(admin_site.urls)),
    (r'^ckeditor/', include('lib.ckeditor.urls')),
    (r'^assunto/(?P<slugcategoria>[\w_-]+)/', include('lib.tagging.urls')),
    (r'^tagging_autocomplete/', include('lib.tagging_autocomplete.urls')),
    (r'^ninfas/(?P<sluguf>[\w_-]+)/$', 'ninfas.views.ninfasestado'),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += patterns('ninfas.views',
     (r'^ninfas/(?P<slugcor>[\w_-]+)/$', 'ninfascor'),
     url(r'^$', 'capa', name='homepage'),

     (r'^ninfas/(?P<sluguf>[\w_-]+)/(?P<slugcidade>[\w_-]+)/(?P<slugninfa>[\w_-]+)/$', 'ninfainterna'),
     (r'^ninfas/(?P<sluguf>[\w_-]+)/(?P<slugcidade>[\w_-]+)/$', 'ninfascidade'),


     (r'^ninfas/$', 'ninfas'),
     (r'^vips/$', 'vips'),

)

if settings.DEBUG:
    #urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # Arquivos enviados pelo usuário
    urlpatterns += staticfiles_urlpatterns()