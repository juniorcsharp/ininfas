# -*- coding: utf-8 -*-
__author__ = 'Junior Lima'

from django.contrib import admin
from yawdadmin import admin_site
from models import Publicidade
from publicidades.models import Cliente


class publicidadeAdmin(admin.ModelAdmin):
    class Media:
        js = ('/js/tiny_mce/tiny_mce.js', '/js/textareas.js')
    fieldsets = [
        ('Principais', {'fields':((('ativo','patrocinado','tipo','nome','cliente'),))}),
        ('Configurações de Data', {'fields':((('data_entrada','data_saida'),))}),
        ('Conteudo', {'fields':('arquivo','url','codigo')}),
        ]
    list_display = ['nome','cliente', 'tipo','url','data_entrada','data_saida','ativo']
    list_filter = ['nome']
    search_fields = ['nome']
    save_on_top= True
    ordering = ['data_entrada']


admin_site.register(Publicidade, publicidadeAdmin)
admin_site.register(Cliente)

