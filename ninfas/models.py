# coding=utf-8
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models

from core.destino_imagem import acompanhante, acompanhante_galeria
from core.thumbs import ImageWithThumbsField

def destino_imagem_galeria(instance, filename):
    return '/'.join(['fotos', str(instance.galeria.slug), filename])

class Cor(models.Model):
    nome = models.CharField(max_length=150)
    descricao = models.TextField(blank=True, null=True)
    slugcor = models.SlugField(blank=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Cor'
        verbose_name_plural = 'Cor'

    def get_absolute_url(self):
        return reverse('ninfas.views.ninfascor', kwargs={'slugcor': self.slugcor})


class Estado(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    uf = models.CharField(max_length=100, blank=True, null=True)
    sluguf = models.SlugField(blank=True)
    slugestado = models.SlugField(blank=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'

class Cidades(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    estado = models.ForeignKey(Estado)
    slugcidade = models.SlugField(blank=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Cidade'
        verbose_name_plural = 'Cidades'

    def get_absolute_url(self):
        return reverse('ninfas.views.ninfascidade', kwargs={'sluguf':self.estado.sluguf,'slugcidade': self.slugcidade})


class Hobby(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    descricao = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Hoob'
        verbose_name_plural = 'Hoobs'

class Musicas(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    descricao = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Música'
        verbose_name_plural = 'Músicas'

class Idiomas(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    descricao = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Idioma'
        verbose_name_plural = 'Idiomas'

class Meses(models.Model):
    nome = models.CharField(max_length=100, unique=True)
    descricao = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Mês'
        verbose_name_plural = 'Meses'

class Acompanhantes(models.Model):
    status = models.BooleanField(default=True)
    destaque = models.BooleanField(blank=True)
    vip = models.BooleanField(blank=True)
    nome = models.CharField(max_length=190, unique=True)
    descricao = models.CharField(max_length=255, blank=True, null=True)
    cor = models.ForeignKey(Cor, blank=True, null=True,default=1)
    cidade = models.ForeignKey(Cidades)
    altura = models.CharField(max_length=100, help_text='1,60cm')
    idade = models.IntegerField(blank=True, null=True)
    hobby = models.ManyToManyField(Hobby,blank=True, null=True)
    musica = models.ManyToManyField(Musicas,blank=True, null=True)
    quadris = models.CharField(max_length=50, help_text='60cm',blank=True, null=True)
    cartao = models.BooleanField('Aceita Cartão?',blank=True)
    visa = models.BooleanField(blank=True)
    master = models.BooleanField(blank=True)
    credishop = models.BooleanField(blank=True)
    viaja = models.BooleanField(blank=True)
    idiomas = models.ManyToManyField(Idiomas,blank=True, null=True)
    contato = models.CharField(max_length=50,help_text='99.9999-9999')
    celular = models.CharField(max_length=50,help_text='99.9999-9999',blank=True, null=True)
    email = models.EmailField(help_text='seuemail@email.com',blank=True, null=True)
    site =  models.URLField(blank=True, null=True)

    imagem_capa = ImageWithThumbsField('Imagem Capa', upload_to=acompanhante, sizes=((800,600),(300,225),(212,318)))
    imagem_destaque = ImageWithThumbsField('Imagem Destaque', upload_to=acompanhante, sizes=((800,600),(212,318)),blank=True,null=True)

    valor = models.FloatField('Valor Mensalidade: R$',help_text='130.00', blank=True, null=True)
    vencimento = models.IntegerField(blank=True, null=True)

    criado_em = models.DateTimeField(auto_now_add=True)
    cliques = models.IntegerField('Cliques', default=0, editable=False)
    slug = models.SlugField(blank=True)

    def __unicode__(self):
        return  self.nome
    class Meta:
        verbose_name = 'Acompanhante'
        verbose_name_plural = 'Acompanhantes'

    def get_absolute_url(self):
        return reverse('ninfas.views.ninfainterna', kwargs={'slugcidade':self.cidade.slugcidade,'sluguf':self.cidade.estado.sluguf,'slugninfa': self.slug})


class Imagem(models.Model):
    foto = ImageWithThumbsField('Imagem Destaque', upload_to=acompanhante_galeria, sizes=((918,573),(170,127)))
    galeriaImagem = models.ForeignKey(Acompanhantes, related_name='galeriaImagem')
    class Meta:
        verbose_name = 'Imagem'
        verbose_name_plural = 'Imagens'


# SIGNALS
from django.db.models import signals, ImageField
from django.template.defaultfilters import slugify

def acompanhante_pre_save(signal, instance, sender, **kwargs):
    instance.slug = slugify(instance.nome)

signals.pre_save.connect(acompanhante_pre_save, sender=Acompanhantes)

def estado_pre_save(signal, instance, sender, **kwargs):
    instance.sluguf = slugify(instance.uf)
    instance.slugestado = slugify(instance.nome)

signals.pre_save.connect(estado_pre_save, sender=Estado)

def cidade_pre_save(signal, instance, sender, **kwargs):
    instance.slugcidade = slugify(instance.nome)

signals.pre_save.connect(cidade_pre_save, sender=Cidades)

def cor_pre_save(signal, instance, sender, **kwargs):
    instance.slugcor = slugify(instance.nome)

signals.pre_save.connect(cor_pre_save, sender=Cor)
