# Create your views here.

from datetime import datetime
from django.template.context import RequestContext,Context
from django.shortcuts import render_to_response, get_object_or_404
from publicidades.models import *
from ninfas.models import *

def capa(request):
    principal = Acompanhantes.objects.filter(destaque__icontains = 1).order_by('-nome')[:1]
    publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    semana = Acompanhantes.objects.filter(destaque__icontains = 0).order_by("-criado_em")
    ninfas = Acompanhantes.objects.filter(destaque__icontains = 0).order_by("?")
    destaque = Acompanhantes.objects.filter(destaque__icontains = 1).order_by("?")
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")
    catcor = Cor.objects.all().order_by("?")[:1]

    return render_to_response('ninfas/home.html',locals(),context_instance=RequestContext(request))


def homepage(request):
    principal = Acompanhantes.objects.filter(destaque__icontains = 1).order_by('-nome')[:1]
    publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    semana = Acompanhantes.objects.filter(destaque__icontains = 0).order_by("-criado_em")
    ninfas = Acompanhantes.objects.filter(destaque__icontains = 0).order_by("?")
    destaque = Acompanhantes.objects.filter(destaque__icontains = 1).order_by("?")
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")

    return render_to_response('ninfas/index.html',locals(),context_instance=RequestContext(request))

def ninfas(request):
    principal = Acompanhantes.objects.filter(destaque__icontains = 1).order_by('-nome')[:1]
    publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    semana = Acompanhantes.objects.filter(destaque__icontains = 0).order_by("-criado_em")
    ninfas = Acompanhantes.objects.filter(destaque__icontains = 0).order_by("?")
    destaque = Acompanhantes.objects.filter(destaque__icontains = 1).order_by("?")
    cidades = Cidades.objects.all().order_by("nome")
    cor = Cor.objects.all().order_by("?")
    catcor = Cor.objects.all().order_by("?")[:1]

    return render_to_response('ninfas/ninfas-geral.html',locals(),context_instance=RequestContext(request))

def vips(request):
    principal = Acompanhantes.objects.filter(destaque__icontains = 1).order_by('-nome')[:1]
    publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    semana = Acompanhantes.objects.filter(destaque__icontains = 0,vip__icontains = 1).order_by("-criado_em")
    ninfas = Acompanhantes.objects.filter(destaque__icontains = 0,vip__icontains = 1).order_by("?")
    destaque = Acompanhantes.objects.filter(destaque__icontains = 1,vip__icontains = 1).order_by("?")
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")
    catcor = Cor.objects.all().order_by("?")[:1]
    return render_to_response('ninfas/vips.html',locals(),context_instance=RequestContext(request))

def ninfainterna(request, slugcidade, sluguf, slugninfa):
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")
    semana = Acompanhantes.objects.filter(cidade__slugcidade=slugcidade,
                                cidade__estado__sluguf=sluguf,destaque__icontains = 0).order_by("-criado_em")
    #blogs = BlogUsuario.objects.filter(blog__ativo__icontains = 1).order_by('-blog')
    #plantao = Noticia.objects.filter(publicar__icontains=1).order_by('-datapublicacao')
    #lidas = Noticia.objects.filter(publicar__icontains=1,subcategoria__categoria__slugcategoria=slugcategoria,subcategoria__slugsubcategoria=slugsubcategoria).order_by('-hits')[:7]
    #team = Times.objects.all().order_by('-nome')
    catcor = Cor.objects.all().order_by("?")[:1]
    ninfa = get_object_or_404(Acompanhantes, cidade__slugcidade=slugcidade,
                                cidade__estado__sluguf=sluguf, slug=slugninfa)
    galeria = Imagem.objects.filter(galeriaImagem__slug=slugninfa)
    #noticias = Noticia.objects.filter(manchete__icontains=0, publicar__icontains=1).order_by('-datapublicacao')
    #subcategoria = SubCategoria.objects.filter(categoria__slugcategoria=slugcategoria)
    publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())

    #relacionada = Noticia.objects.filter(manchete__icontains=0, publicar__icontains=1,
    #                                     subcategoria__categoria__slugcategoria__contains=slugcategoria,subcategoria__slugsubcategoria=slugsubcategoria).order_by(
     #   '-datapublicacao')
    #noti = Noticia.objects.all().order_by('datapublicacao')[:6]

    #artigofooter = Noticia.objects.filter(nomecategoria__id = 5,publicar__icontains=1,manchete__icontains=0).order_by('-datapublicacao')[:3]
    #blogfooter = NoticiaBlog.objects.all().order_by('-datapublicacao').filter(publicar__icontains=1)[:3]
    #ilhainternaquadrada = Publicidade.objects.filter(ativo__exact='1',tipo__exact='7',dataentrada__lte=datetime.now(),datasaida__gte=datetime.now())[:1]
    #ilhainternawide = Publicidade.objects.filter(ativo__exact='1',tipo__exact='8',dataentrada__lte=datetime.now(),datasaida__gte=datetime.now())[:1]
    #objeto = TagItem.objects.filter(object_id=noticias.id)

    #categorias = CategoriaNoticia.objects.all().order_by('nome')
    #subcategorias = SubCategoria.objects.all().order_by('nome')

    if ninfainterna:
        if not request.session.get('ninfa_hits_%s' % slugninfa):
            ninfa.cliques = ninfa.cliques + 1
            ninfa.save()
            request.session['ninfa_hits_%s' % slugninfa] = True

    return render_to_response('ninfas/ninfa-interna.html', locals(), context_instance=RequestContext(request))

def ninfasestado(request, sluguf):
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")
    catcor = Cor.objects.all().order_by("?")[:1]
    #blogs = BlogUsuario.objects.filter(blog__ativo__icontains = 1).order_by('-blog')
    #publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    #lidas = Noticia.objects.filter(publicar__icontains=1,subcategoria__categoria__slugcategoria__contains=slugcategoria).order_by('-hits')[:7]
    #plantao = Noticia.objects.filter(publicar__icontains=1).order_by('-datapublicacao')[:5]
    #team = Times.objects.all().order_by('-nome')

    #noticiasmanchete = Noticia.objects.filter(manchete__icontains=1, publicar__icontains=1,
    #                                          subcategoria__categoria__slugcategoria__contains=slugcategoria).order_by(
    #    '-datapublicacao')[:30]
    ninfas = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf).order_by('-nome')[:30]
    semana = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf).order_by('-nome')[:30]
    destaque = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf,destaque__icontains = 1).order_by('-nome')[:30]

    #ninfas = get_object_or_404(Acompanhantes, cidade__estado__sluguf__contains=sluguf)
    #subcategoria = SubCategoria.objects.filter(categoria__slugcategoria__contains=slugcategoria).order_by('-nome')

    #categorias = CategoriaNoticia.objects.all().order_by('nome')
    #subcategorias = SubCategoria.objects.all().order_by('nome')

    return render_to_response('ninfas/ninfas.html', locals(), context_instance=RequestContext(request))

def ninfascidade(request, sluguf, slugcidade):
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")
    catcor = Cor.objects.all().order_by("?")[:1]
    #blogs = BlogUsuario.objects.filter(blog__ativo__icontains = 1).order_by('-blog')
    #publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    #lidas = Noticia.objects.filter(publicar__icontains=1,subcategoria__categoria__slugcategoria__contains=slugcategoria).order_by('-hits')[:7]
    #plantao = Noticia.objects.filter(publicar__icontains=1).order_by('-datapublicacao')[:5]
    #team = Times.objects.all().order_by('-nome')

    #noticiasmanchete = Noticia.objects.filter(manchete__icontains=1, publicar__icontains=1,
    #                                          subcategoria__categoria__slugcategoria__contains=slugcategoria).order_by(
    #    '-datapublicacao')[:30]

    ninfas = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf,cidade__slugcidade__contains=slugcidade).order_by('-criado_em')
    principal = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf,cidade__slugcidade__contains=slugcidade).order_by('-nome')
    semana = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf,cidade__slugcidade__contains=slugcidade,destaque__icontains = 0).order_by('?')[:4]
    destaque = Acompanhantes.objects.filter(cidade__estado__sluguf__contains=sluguf,cidade__slugcidade__contains=slugcidade,destaque__icontains = 1).order_by('?')

    publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    cidade = get_object_or_404(Cidades, slugcidade__contains=slugcidade)
    #subcategoria = SubCategoria.objects.filter(categoria__slugcategoria__contains=slugcategoria).order_by('-nome')

    #categorias = CategoriaNoticia.objects.all().order_by('nome')
    #subcategorias = SubCategoria.objects.all().order_by('nome')

    return render_to_response('ninfas/ninfas.html', locals(), context_instance=RequestContext(request))

def ninfascor(request, slugcor):
    cidades = Cidades.objects.all().order_by("?")
    cor = Cor.objects.all().order_by("?")
    catcor = Cor.objects.all().order_by("?")[:1]
    #blogs = BlogUsuario.objects.filter(blog__ativo__icontains = 1).order_by('-blog')
    #publicidade = Publicidade.objects.filter(data_entrada__lte=datetime.now(), data_saida__gte=datetime.now())
    #lidas = Noticia.objects.filter(publicar__icontains=1,subcategoria__categoria__slugcategoria__contains=slugcategoria).order_by('-hits')[:7]
    #plantao = Noticia.objects.filter(publicar__icontains=1).order_by('-datapublicacao')[:5]
    #team = Times.objects.all().order_by('-nome')

    #noticiasmanchete = Noticia.objects.filter(manchete__icontains=1, publicar__icontains=1,
    #                                          subcategoria__categoria__slugcategoria__contains=slugcategoria).order_by(
    #    '-datapublicacao')[:30]
    ninfas = Acompanhantes.objects.filter(cor__slugcor=slugcor).order_by('?')[:30]
    semana = Acompanhantes.objects.filter(cor__slugcor=slugcor).order_by('?')[:30]
    destaque = Acompanhantes.objects.filter(cor__slugcor=slugcor,destaque__icontains = 1).order_by('?')[:30]

    #ninfas = get_object_or_404(Acompanhantes, cidade__estado__sluguf__contains=sluguf)
    #subcategoria = SubCategoria.objects.filter(categoria__slugcategoria__contains=slugcategoria).order_by('-nome')

    #categorias = CategoriaNoticia.objects.all().order_by('nome')
    #subcategorias = SubCategoria.objects.all().order_by('nome')

    return render_to_response('ninfas/ninfas.html', locals(), context_instance=RequestContext(request))