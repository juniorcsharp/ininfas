# -*- coding: utf-8 -*-
__author__ = 'Junior Lima'

from django import forms
from django.shortcuts import get_object_or_404
from yawdadmin import admin_site
from yawdadmin.admin_options import OptionSetAdmin, SiteOption

from ninfas.models import *

from django.contrib import admin
from yawdadmin import admin_site
from models import Acompanhantes, Imagem
from mensalidades.models import *
from publicidades.models import *
from lib.multiupload.admin import MultiUploadAdmin
from lib.sorl.thumbnail.admin import AdminImageMixin


class ImagemInlineAdmin(admin.TabularInline):
    model = Imagem
    extra = 10

class AcompanhanteAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Informações',{'fields':(('status','destaque','vip'),('nome','descricao'))}),
        ('Pessoais',{'fields':(('cidade','altura','idade','hobby','cor'),('musica','quadris'),('cartao','visa','master','credishop','viaja'))}),
        ('Outras Informações',{'fields':(('idiomas'),('contato','celular','email','site'),'imagem_capa','imagem_destaque')}),
    ]
    list_display = ['nome','cidade','destaque','status','cliques']
    inlines = [ImagemInlineAdmin]


class MensalidadeAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Informações',{'fields':((('status','acompanhante','valor'),'referente'))}),
    ]
    list_display = ['acompanhante','valor','referente','data_pagamento','status']


admin_site.register(Acompanhantes,AcompanhanteAdmin)
admin_site.register(Estado)
admin_site.register(Cidades)
admin_site.register(Hobby)
admin_site.register(Musicas)
admin_site.register(Idiomas)
admin_site.register(Meses)
admin_site.register(Mensalidades,MensalidadeAdmin)
admin_site.register(Cor)


