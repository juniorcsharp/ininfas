# coding=utf-8
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from ninfas.models import Acompanhantes,Meses
from core.destino_imagem import acompanhante, acompanhante_galeria
from core.thumbs import ImageWithThumbsField


class Mensalidades(models.Model):
    status = models.BooleanField(default=True)
    acompanhante = models.ForeignKey(Acompanhantes)
    data_pagamento = models.DateField(auto_now_add=True)
    valor = models.FloatField('Valor Mensalidade: R$',help_text='130.00')
    referente = models.ForeignKey(Meses,help_text='Referente ao mês de Janeiro')
    usuario = models.ForeignKey(User, related_name='user_add_%(class)ss', blank=True, null=True)
    observacoes = models.TextField(blank=True,null=True)

    def __unicode__(self):
        return  self.acompanhante.nome
    class Meta:
        verbose_name = 'Mensalidade'
        verbose_name_plural = 'Mensalidades'
