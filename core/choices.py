# -*- coding: utf-8 -*-
__author__ = 'Junior Lima'

NOTICIAS_CATEGORIA_EVENTO = (
    ('1','Show'),
    ('2','Palestra'),
    ('3','Congresso'),
    ('4','Retiro'),
    ('5','Culto')
)

PUBLICIDADES_TIPOS = (
    ('1','Show'),
    ('2','Palestra'),
    ('3','Congresso')
)

CAMPEONATO_JOGADOR_C = (
    ('1','Sim, se fosse só um'),
    ('2','Não, o bom é só a galera da igreja'),
    ('3','Tanto faz, te vira Juvenis')
)

CAMPEONATO_QUADRA_C = (
    ('1','Quadra do Almeidão'),
    ('2','Quadra do Corpo de Bombeiros'),
    ('3','Quadra do Tancredo')
)

CAMPEONATO_TURNO_C = (
    ('1','Sim'),
    ('2','Não'),
    ('3','Poderemos ver essa possibilidade')
)

SHOPPING_STATUS_CHOICES = (
    ('1','Ativo'),
    ('2','Finalizado')
)

SHOPPING_SITUACAO_PRODUTO = (
    ('1','Ativo'),
    ('2','Finalizado')
)